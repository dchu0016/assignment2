// Code for the Record Region page.

p = new Region(new Date());

nrL = JSON.parse(localStorage.getItem("APP_PREFIX"));

regList = null;

if (nrL == null)
{
    regList = new RegionList([], 0);
}
else
{
    regList = new RegionList(nrL._regionsArr, nrL._numOfRegions);
}

mapboxgl.accessToken = 'pk.eyJ1Ijoia2F5ZGVubm4iLCJhIjoiY2tkbjMxN3QxMDgzZTJ6cHk1ZTYxNjRzZSJ9.J3m5YCNmEznrKlqecZ3lFA';
    var map = new mapboxgl.Map({
        container: 'map', // container id
        style: 'mapbox://styles/mapbox/streets-v11',
        center: [145.1358874178827, -37.911423354365987], // starting position
        zoom: 18 // starting zoom
    });

    map.addControl(
        new mapboxgl.GeolocateControl({
            positionOptions: {
                enableHighAccuracy: true
            },
            trackUserLocation: true
        })
    );
    
    /*
        saveRegionInstance function
        Function when initiated will save any selected region
    */
    function saveRegionInstance(currentRegion)
    {
        if (typeof(Storage) != "undefined")
        {
            let number = regList.getRegionNum();
            if (number >= 1)
            {
                if (JSON.stringify(regList.getRegion(number - 1)) === JSON.stringify(currentRegion))
                {
                    regList.regionRemove();
                }
            }
            regList.createRegion(currentRegion);
            localStorage.setItem("APP_PREFIX", JSON.stringify(regList));
        }
    }
    
    /*
        cornerAddition functin
        Function to add corner and polygon onto map
    */
    function cornerAddition()
    {
        map.on('click', function(e) {
            p.addCorner([e.lngLat.lng, e.lngLat.lat]);
            createPolygon(p, true);
            displayMessage("Corner Added", 1000)
            saveRegionInstance(p);
        });
    }

    /*
        resetRegion function
        Function to delete selected corners
    */
    function resetRegion()
    {
        let deleteSelectedArea = p.deleteCorner();
        createPolygon(p, true);
        saveRegionInstance(p);
    }
