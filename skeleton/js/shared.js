// Shared code needed by the code of all three pages.
// Prefix to use for Local Storage.  You may change this.
var APP_PREFIX = "monash.eng1003.fencingApp";

// Array of saved Region objects.
var savedRegions = [];

// Array of markers on the map
var markerArr = [];

// getting distance between fence from local storage
var distanceFence = parseFloat(localStorage.getItem(APP_PREFIX + "-selectedValue"));

// Region Class with methods
class Region
{
	constructor(time)
    {
		this._cornerArr = [];
		this._time = time;
	}

	get cornerArr()
    {
		return this._cornerArr;
	}

	get time()
    {
		return this._time;
	}

	set cornerArr(newCorner)
    {
		this._cornerArr = newCorner;
	}

	set time(newTime)
    {
		this._time = newTime;
	}

	addCorner(corner)
    {
		if(corner != null)
        {
			this._cornerArr.push(corner);
		}
	}

	deleteCorner()
    {
		return this._cornerArr.splice(0);
	}

	calculateAnP()
    {
        let geod = GeographicLib.Geodesic.WGS84;
		let p = geod.Polygon(false);
		for (let i = 0; i < this._cornerArr.length; i++) 
        {
			let lnglat = this._cornerArr[i];
			p.AddPoint(lnglat[1],lnglat[0]);
		}
		let aP = p.Compute(false,true);
        if (aP.area < 0)
        {
            return "Area: " + (-1 * aP.area.toFixed(2)) + " m^2" + "<br> Perimeter: " + aP.perimeter.toFixed(2) + " m"
        }
        else if(aP.area >= 0)
        {
            return "Area: " + aP.area.toFixed(2) + " m^2" + "<br> Perimeter: " + aP.perimeter.toFixed(2) + " m"
        }

	}

	boundaryFencePosts(lat1, lon1, lat2, lon2, distanceFence)
	{
        let geod = GeographicLib.Geodesic.WGS84;
		var inv1 = geod.Inverse(lat1, lon1, lat2, lon2);
		var inv2 = geod.Inverse(lat1, lon1, lat2, lon2, GeographicLib.Geodesic.AZIMUTH);
        var number = 1;
        while (inv1.s12/number > distanceFence)
        {
            number++;
        }
        let distance = inv1.s12/number
        let position = [];
        position.push([lon1, lat1]);
        for (let i = 0; i < number - 1; i++)
        {
            let finalPosition = position[position.length - 1];
            let direct = geod.Direct(finalPosition[1], finalPosition[0], inv2.azi1, distance);
            position.push([direct.lon2, direct.lat2]);
        }
        position.push([lon2, lat2]);
        return position
	}

    fencePositions()
    {
        let positionsOfFence = [];
        let mark = this._cornerArr.slice();
        if (mark != null && mark.length != 0)
        {
            mark.push(mark[0]);
            for (let i = 0; i < mark.length - 1; i++)
            {
                let currentPositions = this.boundaryFencePosts(mark[i][1], mark[i][0], mark[i+1][1], mark[i+1][0], distanceFence);
                positionsOfFence = positionsOfFence.concat(currentPositions);
            }
        }
        return positionsOfFence;
    }

	toString()
    {
		return this._cornerArr.toString() + "," + this._time.toString();
	}
}

// Feature 2: creating RegionList class
class RegionList
{
    constructor(regionsArr, numOfRegions)
    {
        this._regionsArr = regionsArr;
        this._numOfRegions = numOfRegions;
    }

    getRegion(number)
    {
        if (number >= 0 && number < this._regionsArr.length)
        {
            return this._regionsArr[number];
        }
    }
    
    getRegionNum()
    {
        return this._numOfRegions;
    }
    
    createRegion(region)
    {
        this._regionsArr.push(region);
        this._numOfRegions++;
    }

    deleteRegion(number)
    {
        if (number >= 0 && number < this._regionsArr.length)
        {
            
            let p = this._regionsArr[number];

            if (number != this._regionsArr.length - 1)
            {
                this._regionsArr = this._regionsArr.slice(0, number).concat(this._regionsArr.slice(number + 1));
            }
            else
            {
                this._regionsArr = this._regionsArr.slice(0, number);
            }

            this._numOfRegions--;
            return p;
        }
    }

    regionRemove()
    {
        if (this._numOfRegions >= 1)
        {
            this.deleteRegion(this._numOfRegions - 1);
        }
    }

    toString()
    {
        return this._regionsArr.toString();
    }
}

/*
    newRegionList function
    Function that initiates a list when a polygon is saved
*/
function newRegionList()
{
    nrL = JSON.parse(localStorage.getItem("APP_PREFIX"));
    if (nrL != null)
    {
        regList = new RegionList([], 0);
        for (let i = 0; i < nrL._numOfRegions; i++)
        {
            let reg = nrL._regionsArr[i];
            if (reg != null)
            {
                regionTemp = new Region(reg._time);
                regionTemp.cornerArr = reg._cornerArr;
                regList.createRegion(regionTemp);
            }
        }
        return regList;
    }
}

/*
    createPolygon function
    Function that marks the corner as well as selected area
*/
function createPolygon(currentRegion, markerAppearance)
{
    layerRemove('regionID');
    let rg = currentRegion.cornerArr.slice();
    if (markerAppearance)
    {
        for (i = 0; i < markerArr.length; i++)
        {
            markerArr[i].remove();
        }
        for (i = 0; i < rg.length; i++)
        {
            var marker = new mapboxgl.Marker().setLngLat(rg[i]).addTo(map);
            markerArr.push(marker);
        }
    }
    initialCorner = rg[0];
    rg.push(initialCorner);
    coordinate = [];
    for (i = 0; i < rg.length; i++)
    {
        lnglat = rg[i];
        coordinate.push(lnglat);
    }
    map.addSource('regionID', {
        'type': 'geojson',
        'data': {
            'type': 'Feature',
            'geometry': {
                'type': 'Polygon',
                'coordinates': [coordinate]
            }
        }
    });

    map.addLayer({
        'id': 'regionID',
        'type': 'fill',
        'source': 'regionID',
        'layout': {},
        'paint': {
            'fill-color': '#48D1CC',
            'fill-opacity': 0.8
        }
    });
}

/*
    polygonList function
    Function stores list of regions saved
*/
function polygonList(rgnList)
{
    layerRemove('rgnListID');
    mapFeatures = [];
    let number = rgnList.getRegionNum();
    for ( let i = 0; i < number; i++)
    {
        let reg = rgnList.getRegion(i);
        if (reg != null)
        {
            regionTemp = new Region(reg._time);
            regionTemp.cornerArr = reg._cornerArr;
            let rg = regionTemp.cornerArr.slice();
        }
    }
}

/* 
    layerRemove function
    Function to remove any known layer created on the map
*/
function layerRemove(toRemove)
{
    let yesPoly = map.getLayer(toRemove)
    if (yesPoly !== undefined)
    {
        map.removeLayer(toRemove);
        map.removeSource(toRemove);
    }
}

/*
    fenceMarkers function
    Function to display the position of fence
*/
function fenceMarkers(currentRegion, markerPosition, markerAppearance, showFence)
{
    layerRemove('rgListID');
    layerRemove('circleID');
    let rg = currentRegion.cornerArr.slice();
    initialCorner = rg[0];
    rg.push(initialCorner);
    coordinate = [];
    for (i = 0; i < rg.length; i++)
    {
        lnglat = rg[i];
        coordinate.push(lnglat);
    }
    mapFeatures = [];
    let polygonFeature = {
        'type': 'Feature',
        'geometry': {
            'type': 'Polygon',
            'coordinates': [coordinate]
        }
    }
    mapFeatures.push(polygonFeature);
    if (showFence)
    {
        for (let j = 0; j < markerPosition.length; j++)
        {
            let pointFeature = {
                'type': 'Feature',
                'geometry': {
                    'type': 'Point',
                    'coordinates': markerPosition[j]
                }
            }
            mapFeatures.push(pointFeature);
        }
    }
    map.addSource('rgListID', {
        'type': 'geojson',
        'data': {
            'type': 'FeatureCollection',
            'features': mapFeatures
        }
    });
    map.addLayer({
        'id': 'rgListID',
        'type': 'fill',
        'source': 'rgListID',
        'layout': {},
        'paint': {
            'fill-color': '#48D1CC',
            'fill-opacity': 0.8
        }
    });
    if (showFence)
    {
        map.addLayer({
            'id': 'circleID',
            'type': 'circle',
            'source': 'rgListID',
            'paint': {
                'circle-radius': 10, 
                'circle-color': '#0000ff'
            },
            'filter': ['==', '$type', 'Point']
        });
    }
}