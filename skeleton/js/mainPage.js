// Code for the main app page (Regions List).

function viewRegion(regionIndex)
{
    // Save the desired region to local storage so it can be accessed from view region page.
    localStorage.setItem(APP_PREFIX + "-selectedRegion", regionIndex);
    // ... and load the view region page.
    location.href = 'viewRegion.html';
}

mapboxgl.accessToken = 'pk.eyJ1Ijoia2F5ZGVubm4iLCJhIjoiY2tkbjMxN3QxMDgzZTJ6cHk1ZTYxNjRzZSJ9.J3m5YCNmEznrKlqecZ3lFA';
    var map = new mapboxgl.Map({
        container: 'map', // container id
        style: 'mapbox://styles/mapbox/streets-v11',
        center: [145.1358874178827, -37.911423354365987], // starting position
        zoom: 13 // starting zoom
    });

    /*
        createRegionList function
        Function serves as a display of saved region and timed saved in mainPage
    */
    function createRegionList(regList)
    {
        let aP = "";
        for (let i = 0; i < regList.getRegionNum(); i++)
        {
            let num = i.toString();
            let time = regList.getRegion(i)._time;
            let date = time.toString('en-US', { timeZone: 'Asia/Singapore' }).replace('T', ' @').replace('Z', '')
            aP += `<li class=\"mdl-list__item mdl-list__item--two-line\" onclick=\"viewRegion(${num});\">`
            aP += `<span class=\"mdl-list__item-primary-content\">`
            aP += `<span>Saved Region ${num}</span>`
            aP += `<span class=\"mdl-list__item-sub-title\"> ${date}</span> </span> </li>`
        }
        return aP;
    }

    map.on('load', function () {
        nrL = JSON.parse(localStorage.getItem("APP_PREFIX"));
        if (nrL != null)
        {
            regList = new RegionList(nrL._regionsArr, nrL._numOfRegions);
            polygonList(regList, false);
            document.getElementById("regionsList").innerHTML = createRegionList(regList);
        }
    });
