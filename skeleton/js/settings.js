// Code is for settings page

/*
    setFenceValue function
    Function to grab value input from user
*/
function setFenceValue()
{
	inputFenceDistance = document.getElementById('inputValue').value;
	fencePostsDistance = (inputFenceDistance*10)/10;
	localStorage.setItem(APP_PREFIX + "-selectedValue", JSON.stringify(fencePostsDistance))
	return fencePostsDistance
}

/*
    getFenceValue function
    Function to display selected value
*/ 
function getFenceValue()
{
	selectedDistance = setFenceValue()
	document.getElementById('distance').innerHTML = "Selected Value: " + selectedDistance + "m";
	return selectedDistance
}

