// Code for the View Region page.

// The following is sample code to demonstrate navigation.
// You need not use it for final app.

var regionIndex = localStorage.getItem(APP_PREFIX + "-selectedRegion");
if (regionIndex !== null)
{
    // If a region index was specified, show name in header bar title. This
    // is just to demonstrate navigation.  You should set the page header bar
    // title to an appropriate description of the region being displayed.
    document.getElementById("headerBarTitle").textContent = "Regions " + regionIndex;
}
mapboxgl.accessToken = 'pk.eyJ1Ijoia2F5ZGVubm4iLCJhIjoiY2tkbjMxN3QxMDgzZTJ6cHk1ZTYxNjRzZSJ9.J3m5YCNmEznrKlqecZ3lFA';
    var map = new mapboxgl.Map({
        container: 'map', // container id
        style: 'mapbox://styles/mapbox/streets-v11',
        center: [145.1358874178827, -37.911423354365987], // starting position
        zoom: 18 // starting zoom
    });

    map.on('load', function () {

        nrL = JSON.parse(localStorage.getItem("APP_PREFIX"));
        if(nrL != null){
            regList = new RegionList(nrL._regionsArr,nrL._numOfRegions);
            // let rgn = rgList.getRegion(regionIndex);
            let reg = regList.getRegion(Number(regionIndex));
            if(reg != null){
                regionTemp = new Region(reg._time);
                regionTemp.cornerArr = reg._cornerArr;
                document.getElementById("calculations").innerHTML = regionTemp.calculateAnP();
                markerPosition = regionTemp.fencePositions();
                let showFence = getFence();
                fenceMarkers(regionTemp, markerPosition, false, showFence)
            }
        }

        map.on('click', function(e) {
            removeRegionMessage('Remove Region', 2000);
        });
    });

    /*
        removeRegion function
        Function to delete chosen region from local storage
    */
    function removeRegion()
    {
        // console.log("function removeRegion");
        regList = newRegionList();
        // console.log(rgList);
        if(regList != null)
        {
            if(typeof(Storage) != "undefined"){
                let num = regList.getRegionNum();
                if(num >= 1)
                {
                    regList.deleteRegion(Number(regionIndex));
                }
                localStorage.setItem("APP_PREFIX",JSON.stringify(regList));

                location.href = 'index.html'
            }
        }
    }
    
    /*
        getFence function
        Function would retrieve fence positions and place it on the polygon
    */
    function getFence()
    {
        let showFence = false;
        if (localStorage.getItem(APP_PREFIX + "-fence") == null)
        {
            localStorage.setItem(APP_PREFIX + "-fence", JSON.stringify(false));
        }
        let fenceString = localStorage.getItem(APP_PREFIX + "-fence");
        if (fenceString == 'true')
        {
            showFence = true;
        }
        else if (fenceString == 'false')
        {
            showFence = false;
        }
        localStorage.setItem(APP_PREFIX + "-fence", JSON.stringify(showFence));
        return showFence;
    }

    /*
        toggleFencePosts function
        Function would turn fence markers on and off
    */
    function toggleFencePosts()
    {
        let showFence = getFence();
        localStorage.setItem(APP_PREFIX + "-fence", JSON.stringify(!showFence));
        location.reload();
    }
